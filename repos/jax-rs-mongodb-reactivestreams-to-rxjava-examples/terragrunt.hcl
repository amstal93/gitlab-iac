terraform {
  source = "${get_parent_terragrunt_dir()}/../modules//repo"
}

inputs = {
  repo_name       = "${basename(get_terragrunt_dir())}"
  repo_definition = yamldecode(file("${get_terragrunt_dir()}/final.yaml"))
}

include {
  path = find_in_parent_folders()
}
