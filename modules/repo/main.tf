/*
  MAIN REPOSITORY
*/
resource "gitlab_project" "project" {
  lifecycle {
    prevent_destroy = true
    ignore_changes = [import_url]
  }
  path             = var.repo_name
  name             = try(var.repo_definition.name, var.repo_name)
  namespace_id     = try(var.repo_definition.owner, null)
  archived         = try(var.repo_definition.archived, false)
  visibility_level = try(var.repo_definition.visibility_level, null)
  description      = try(var.repo_definition.description, null)
  tags             = try(var.repo_definition.tags, null)

  default_branch = try(var.repo_definition.default_branch, null)

  mirror                              = try(var.repo_definition.pull_mirror, null)
  import_url                          = try(var.repo_definition.pull_mirror_import_url, null)
  mirror_trigger_builds               = try(var.repo_definition.pull_mirror_trigger_builds, null)
  mirror_overwrites_diverged_branches = try(var.repo_definition.pull_mirror_overwrites_diverged_branches, null)
  only_mirror_protected_branches      = try(var.repo_definition.pull_mirror_only_protected_branches, null)

  request_access_enabled = try(var.repo_definition.request_access_enabled, false)

  issues_enabled             = try(var.repo_definition.issues_enabled, true)
  merge_requests_enabled     = try(var.repo_definition.merge_requests_enabled, true)
  pipelines_enabled          = try(var.repo_definition.pipelines_enabled, true)
  wiki_enabled               = try(var.repo_definition.wiki_enabled, true)
  snippets_enabled           = try(var.repo_definition.snippets_enabled, true)
  container_registry_enabled = try(var.repo_definition.container_registry_enabled, true)
  lfs_enabled                = try(var.repo_definition.lfs_enabled, true)
  packages_enabled           = try(var.repo_definition.packages_enabled, true)

  pages_access_level = try(var.repo_definition.pages_access_level, "private")

  merge_method                                     = try(var.repo_definition.merge_method, "merge")
  approvals_before_merge                           = try(var.repo_definition.approvals_before_merge, 0)
  only_allow_merge_if_pipeline_succeeds            = try(var.repo_definition.only_allow_merge_if_pipeline_succeeds, false)
  only_allow_merge_if_all_discussions_are_resolved = try(var.repo_definition.only_allow_merge_if_all_discussions_are_resolved, false)
  remove_source_branch_after_merge                 = try(var.repo_definition.remove_source_branch_after_merge, true)
  squash_option                                    = try(var.repo_definition.squash_option, "default_off")

  shared_runners_enabled = try(var.repo_definition.shared_runners_enabled, true)
  initialize_with_readme = try(var.repo_definition.initialize_with_readme, false)

  dynamic "push_rules" {
    for_each = try(var.repo_definition.push_rules, null) != null ? ["1"] : []
    content {
      commit_committer_check        = try(var.repo_definition.push_rules.commit_committer_check, false)
      deny_delete_tag               = try(var.repo_definition.push_rules.deny_delete_tag, false)
      member_check                  = try(var.repo_definition.push_rules.member_check, false)
      prevent_secrets               = try(var.repo_definition.push_rules.prevent_secrets, true)
      reject_unsigned_commits       = try(var.repo_definition.push_rules.reject_unsigned_commits, false)
      max_file_size                 = try(var.repo_definition.push_rules.max_file_size, null)
      author_email_regex            = try(var.repo_definition.push_rules.author_email_regex, null)
      branch_name_regex             = try(var.repo_definition.push_rules.branch_name_regex, null)
      commit_message_regex          = try(var.repo_definition.push_rules.membecommit_message_regexr_check, null)
      commit_message_negative_regex = try(var.repo_definition.push_rules.commit_message_negative_regex, null)
      file_name_regex               = try(var.repo_definition.push_rules.file_name_regex, null)
    }
  }

  use_custom_template             = try(var.repo_definition.use_custom_template, false)
  template_name                   = try(var.repo_definition.template_name, null)
  template_project_id             = try(var.repo_definition.template_project_id, null)
  group_with_project_templates_id = try(var.repo_definition.group_with_project_templates_id, null)

  ci_config_path = try(var.repo_definition.ci_config_path, null)
}

/*
  BRANCH PROTECTIONS
*/
resource "gitlab_branch_protection" "project_branch_protections" {
  for_each = {
    for branch_protection in try(var.repo_definition.branch_protections, []) :
    branch_protection.name => {
      push_access_level : branch_protection.push_access_level
      merge_access_level : branch_protection.merge_access_level
      code_owner_approval_required : try(branch_protection.code_owner_approval_required, null)
    }
  }
  project                      = gitlab_project.project.id
  branch                       = each.key
  push_access_level            = each.value.push_access_level
  merge_access_level           = each.value.merge_access_level
  code_owner_approval_required = each.value.code_owner_approval_required
}

/*
  USER MEMBERSHIPS
*/
resource "gitlab_project_membership" "project_memberships" {
  for_each = {
    for user in try(var.repo_definition.users, []) :
    user.username => {
      user_id : data.gitlab_user.project_membership_users[user.username].id
      access_level : user.access_level
    }
    if user.username != var.owner
  }
  project_id   = gitlab_project.project.id
  user_id      = each.value.user_id
  access_level = each.value.access_level
}
data "gitlab_user" "project_membership_users" {
  for_each = {
    for user in try(var.repo_definition.users, []) :
    user.username => user.username
  }
  username = each.value
}

/*
  GROUPS MEMBERSHIPS
*/
resource "gitlab_project_share_group" "project_groups" {
  for_each = {
    for group in try(var.repo_definition.groups, []) :
    group.path => {
      group_id     = data.gitlab_group.project_membership_groups[group.path].id
      access_level = group.access_level
    }
  }
  project_id   = gitlab_project.project.id
  group_id     = each.value.group_id
  access_level = each.value.access_level
}
data "gitlab_group" "project_membership_groups" {
  for_each = {
    for group in try(var.repo_definition.groups, []) :
    group.path => group.path
  }
  full_path = each.value
}

/*
  PROJECT MERGE REQUEST APPROVALS
*/
resource "gitlab_project_level_mr_approvals" "project_mr_approvals" {
  project_id                                     = gitlab_project.project.id
  reset_approvals_on_push                        = try(length(var.repo_definition.reset_approvals_on_push), true)
  disable_overriding_approvers_per_merge_request = try(length(var.repo_definition.disable_overriding_approvers_per_merge_request), true)
  merge_requests_author_approval                 = try(length(var.repo_definition.merge_requests_author_approval), true)
  merge_requests_disable_committers_approval     = try(length(var.repo_definition.merge_requests_disable_committers_approval), false)
}

/*
  VARIABLES
*/
resource "gitlab_project_variable" "project_variable" {
  for_each = {
    for variable in try(var.repo_definition.variables, []) :
    variable.key => {
      value : variable.value
      masked : variable.masked
      protected : try(variable.protected, true)
      environment_scope : try(variable.environment_scope, "*")
    }
  }
  project           = gitlab_project.project.id
  key               = each.key
  value             = each.value.value
  masked            = each.value.masked
  protected         = each.value.protected
  environment_scope = each.value.environment_scope
}

/*
  PUSH MIRRORS
*/
resource "gitlab_project_mirror" "project_push_mirrors" {
  for_each = {
    for push_mirror in try(var.repo_definition.push_mirrors, []) :
    replace(push_mirror.url, try(regex("https?://.+:.+@", push_mirror.url), ""), "") => {
      url : push_mirror.url
      enabled : try(push_mirror.enabled, true)
      only_protected_branches : try(push_mirror.only_protected_branches, true)
      keep_divergent_refs : try(push_mirror.keep_divergent_refs, false)
    }
  }
  lifecycle {
    ignore_changes = [url]
  }
  project                 = gitlab_project.project.id
  url                     = each.value.url
  enabled                 = each.value.enabled
  only_protected_branches = each.value.only_protected_branches
  keep_divergent_refs     = each.value.keep_divergent_refs
}

/*
  TRIGGERS
*/
resource "gitlab_pipeline_trigger" "project_triggers" {
  for_each = {
    for trigger in try(var.repo_definition.triggers, []) :
    trigger.description => trigger.description
  }
  project     = gitlab_project.project.id
  description = each.value
}

/*
  SCHEDULES
*/
resource "gitlab_pipeline_schedule" "project_schedules" {
  for_each = {
    for schedule in try(var.repo_definition.schedules, []) :
    schedule.description => {
      ref : schedule.ref
      cron : schedule.cron
      cron_timezone : try(schedule.cron_timezone, "UTC")
      active : try(schedule.active, true)
    }
  }
  project       = gitlab_project.project.id
  description   = each.key
  ref           = each.value.ref
  cron          = each.value.cron
  cron_timezone = each.value.cron_timezone
  active        = each.value.active
}
resource "gitlab_pipeline_schedule_variable" "project_schedule_variables" {
  for_each = {
    for schedule_variable in local.all_project_schedule_variables :
    "${schedule_variable.schedule}/${schedule_variable.key}" => {
      pipeline_schedule_id = gitlab_pipeline_schedule.project_schedules[schedule_variable.schedule].id
      key : schedule_variable.key
      value : schedule_variable.value
    }
  }
  project              = gitlab_project.project.id
  pipeline_schedule_id = each.value.pipeline_schedule_id
  key                  = each.value.key
  value                = each.value.value
}

/*
  WEBHOOKS
*/
resource "gitlab_project_hook" "project_hooks" {
  count                      = try(length(var.repo_definition.hooks), 0)
  project                    = gitlab_project.project.id
  url                        = sensitive(var.repo_definition.hooks[count.index].url)
  enable_ssl_verification    = try(var.repo_definition.hooks[count.index].enable_ssl_verification, true)
  push_events                = try(var.repo_definition.hooks[count.index].push_events, false)
  push_events_branch_filter  = try(var.repo_definition.hooks[count.index].push_events_branch_filter, null)
  issues_events              = try(var.repo_definition.hooks[count.index].issues_events, false)
  confidential_issues_events = try(var.repo_definition.hooks[count.index].confidential_issues_events, false)
  merge_requests_events      = try(var.repo_definition.hooks[count.index].merge_requests_events, false)
  tag_push_events            = try(var.repo_definition.hooks[count.index].tag_push_events, false)
  note_events                = try(var.repo_definition.hooks[count.index].note_events, false)
  confidential_note_events   = try(var.repo_definition.hooks[count.index].confidential_note_events, false)
  job_events                 = try(var.repo_definition.hooks[count.index].job_events, false)
  pipeline_events            = try(var.repo_definition.hooks[count.index].pipeline_events, false)
  wiki_page_events           = try(var.repo_definition.hooks[count.index].wiki_page_events, false)
  deployment_events          = try(var.repo_definition.hooks[count.index].deployment_events, false)
}

/*
  DEPLOY KEYS
*/
resource "gitlab_deploy_key" "project_deploy_keys" {
  for_each = {
    for deploy_key in try(var.repo_definition.deploy_keys, []) :
    deploy_key.title => {
      key : deploy_key.key
      protected : try(deploy_key.can_push, false)
    }
  }
  project  = gitlab_project.project.id
  title    = each.key
  key      = each.value.key
  can_push = each.value.protected
}

/*
  DEPLOY TOKENS (not managed until there is an safer & easier way to print out the generated token)

resource "gitlab_deploy_token" "project_deploy_tokens" {
  count      = try(length(var.repo_definition.deploy_tokens), 0)
  project    = gitlab_project.project.id
  name       = var.repo_definition.deploy_tokens[count.index].name
  username   = var.repo_definition.deploy_tokens[count.index].name
  scopes     = try(var.repo_definition.deploy_tokens[count.index].scopes, ["read_repository"])
  expires_at = try(var.repo_definition.deploy_tokens[count.index].expires_at, null)
}
*/

/*
  GITHUB INTEGRATION
*/
resource "gitlab_service_github" "github" {
  count          = try(var.repo_definition.github_integration, null) != null ? 1 : 0
  project        = gitlab_project.project.id
  token          = var.repo_definition.github_integration.token
  repository_url = "https://github.com/${var.repo_definition.github_integration.path}"
  static_context = try(var.repo_definition.github_integration.static_context, true)
}

/*
  SELF-TRIGGERS
*/
resource "gitlab_pipeline_trigger" "project_self_triggers" {
  count       = try(length(var.repo_definition.self_triggers), 0)
  project     = gitlab_project.project.id
  description = "SELF TRIGGER: ${var.repo_definition.self_triggers[count.index].branch}${try(var.repo_definition.self_triggers[count.index].description, null) != null ? " - ${var.repo_definition.self_triggers[count.index].description}" : ""}"
}
resource "gitlab_project_hook" "project_self_hooks" {
  count                      = try(length(var.repo_definition.self_triggers), 0)
  project                    = gitlab_project.project.id
  url                        = "https://gitlab.com/api/v4/projects/${gitlab_project.project.id}/ref/${var.repo_definition.self_triggers[count.index].branch}/trigger/pipeline?token=${sensitive(gitlab_pipeline_trigger.project_self_triggers[count.index].token)}"
  enable_ssl_verification    = true
  push_events                = try(var.repo_definition.self_triggers[count.index].push_events, false)
  push_events_branch_filter  = try(var.repo_definition.self_triggers[count.index].push_events_branch_filter, null)
  issues_events              = try(var.repo_definition.self_triggers[count.index].issues_events, false)
  confidential_issues_events = try(var.repo_definition.self_triggers[count.index].confidential_issues_events, false)
  merge_requests_events      = try(var.repo_definition.self_triggers[count.index].merge_requests_events, false)
  tag_push_events            = try(var.repo_definition.self_triggers[count.index].tag_push_events, false)
  note_events                = try(var.repo_definition.self_triggers[count.index].note_events, false)
  confidential_note_events   = try(var.repo_definition.self_triggers[count.index].confidential_note_events, false)
  job_events                 = try(var.repo_definition.self_triggers[count.index].job_events, false)
  pipeline_events            = try(var.repo_definition.self_triggers[count.index].pipeline_events, false)
  wiki_page_events           = try(var.repo_definition.self_triggers[count.index].wiki_page_events, false)
  deployment_events          = try(var.repo_definition.self_triggers[count.index].deployment_events, false)
}

locals {
  all_project_schedule_variables = flatten([
    for _, schedule in try(var.repo_definition.schedules, []) : [
      for variable in try(schedule.variables, []) : {
        schedule = schedule.description
        key      = variable.key
        value    = variable.value
      }
    ]
  ])
}
