variable "owner" {
  type = string
}

variable "repo_name" {
  type = string
}

variable "repo_definition" {
  type = any
}
