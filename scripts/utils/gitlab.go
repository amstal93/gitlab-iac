package utils

import (
	"encoding/json"
	"fmt"
	"gitlab-iac/scripts/models/responses"
	"io/ioutil"
	"log"
	"net/http"
)

var httpClient = &http.Client{}

var _TOKEN_KEY = "PRIVATE-TOKEN"

var _GITLAB_URL = "https://gitlab.com/api/v4/"
var _GITLAB_PATH_PROJECT = "projects/%s"
var _GITLAB_PATH_MEMBERS = "projects/%s/members"
var _GITLAB_PATH_PROTECTED_BRANCHES = "projects/%s/protected_branches"
var _GITLAB_PATH_VARIABLES = "projects/%s/variables"
var _GITLAB_PATH_PIPELINE_SCHEDULES = "projects/%s/pipeline_schedules"
var _GITLAB_PATH_MIRRORS = "projects/%s/remote_mirrors"
var _GITLAB_PATH_DEPLOY_KEYS = "projects/%s/deploy_keys"
var _GITLAB_PATH_SUCCESS_JOBS = "projects/%s/jobs?scope=success"

func _getGitlab(path string) ([]byte, error) {
	req, err := http.NewRequest("GET", _GITLAB_URL+path, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(_TOKEN_KEY, GetGitlabToken())
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return body, nil
}
func GetGitlabProject(projectID string) (responses.Project, error) {
	project := responses.Project{}
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_PROJECT, projectID))
	if err != nil {
		return project, err
	}
	err = json.Unmarshal(body, &project)
	if err != nil {
		log.Println(projectID)
		log.Println(string(body))
		return project, err
	}
	return project, nil
}
func GetGitlabProjectMembers(projectID string) (responses.Members, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_MEMBERS, projectID))
	if err != nil {
		return nil, err
	}
	members := responses.Members{}
	err = json.Unmarshal(body, &members)
	if err != nil {
		log.Println(projectID)
		log.Println(string(body))
		return nil, err
	}
	return members, nil
}
func GetGitlabProjectProtectedBranches(projectID string) (responses.ProtectedBranches, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_PROTECTED_BRANCHES, projectID))
	if err != nil {
		return nil, err
	}
	protectedBranches := responses.ProtectedBranches{}
	err = json.Unmarshal(body, &protectedBranches)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return protectedBranches, nil
}
func GetGitlabVariables(projectID string) (responses.Variables, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_VARIABLES, projectID))
	if err != nil {
		return nil, err
	}
	vars := responses.Variables{}
	err = json.Unmarshal(body, &vars)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return vars, nil
}
func GetGitlabPipelineSchedules(projectID string) (responses.PipelineSchedules, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_PIPELINE_SCHEDULES, projectID))
	if err != nil {
		return nil, err
	}
	scheds := responses.PipelineSchedules{}
	err = json.Unmarshal(body, &scheds)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return scheds, nil
}
func GetGitlabMirrors(projectID string) (responses.Mirrors, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_MIRRORS, projectID))
	if err != nil {
		return nil, err
	}
	mirrors := responses.Mirrors{}
	err = json.Unmarshal(body, &mirrors)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return mirrors, nil
}
func GetGitlabDeployKeys(projectID string) (responses.DeployKeys, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_DEPLOY_KEYS, projectID))
	if err != nil {
		return nil, err
	}
	deployKeys := responses.DeployKeys{}
	err = json.Unmarshal(body, &deployKeys)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return deployKeys, nil
}
func GetGitlabSuccessJobs(projectID string) (responses.Jobs, error) {
	body, err := _getGitlab(fmt.Sprintf(_GITLAB_PATH_SUCCESS_JOBS, projectID))
	if err != nil {
		return nil, err
	}
	jobs := responses.Jobs{}
	err = json.Unmarshal(body, &jobs)
	if err != nil {
		log.Println(string(body))
		return nil, err
	}
	return jobs, nil
}
