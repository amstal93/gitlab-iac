package utils

import (
	"os"
	"regexp"
	"strings"
)

func GetChangedProjects() ([]string, error) {
	changes := make([]string, 0)
	target := "*"
	ciMRTargetBranch := os.Getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME")
	if ciMRTargetBranch != "" {
		target = "origin/" + ciMRTargetBranch
	} else {
		jobs, err := GetGitlabSuccessJobs(GetIACProjectID())
		if err != nil {
			return nil, err
		}
		for _, job := range jobs {
			if job.Stage == "apply" {
				target = job.Pipeline.SHA
				break
			}
		}
	}
	if target == "*" {
		return GetProjectFolders(), nil
	}
	stdout, err := Cmd(GetRepoDir(), "git", "diff", "--name-status", target, "HEAD")
	if err != nil {
		return nil, err
	}
	for _, line := range strings.Split(stdout, "\n") {
		if line != "" {
			split := regexp.MustCompile(`\s+`).Split(line, -1)
			status := split[0]
			path := strings.TrimSpace(split[len(split)-1])
			if status != "D" && strings.HasPrefix(path, "repos/") && strings.HasSuffix(path, "definition.yaml") {
				path = strings.TrimPrefix(path, "repos/")
				path = strings.TrimSuffix(path, "/definition.yaml")
				if !strings.HasPrefix(path, ".") {
					changes = append(changes, path)
				}
			}
		}
	}
	return changes, nil
}
