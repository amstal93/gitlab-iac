package models

type State struct {
	Values struct {
		RootModule struct {
			Resources []struct {
				Type   string `json:"type"`
				Values struct {
					ID               string `json:"id"`
					Archived         bool   `json:"archived"`
					PipelinesEnabled bool   `json:"pipelines_enabled"`
				} `json:"values"`
			} `json:"resources"`
		} `json:"root_module"`
	} `json:"values"`
}

func (state *State) GetProjectID() string {
	for _, resource := range state.Values.RootModule.Resources {
		if resource.Type == "gitlab_project" {
			return resource.Values.ID
		}
	}
	return ""
}

func (state *State) IsArchived() bool {
	for _, resource := range state.Values.RootModule.Resources {
		if resource.Type == "gitlab_project" {
			return resource.Values.Archived
		}
	}
	return false
}

func (state *State) IsPipelinesEnabled() bool {
	for _, resource := range state.Values.RootModule.Resources {
		if resource.Type == "gitlab_project" {
			return resource.Values.PipelinesEnabled
		}
	}
	return false
}
