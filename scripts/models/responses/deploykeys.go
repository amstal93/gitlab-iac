package responses

type DeployKeys []struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
}
