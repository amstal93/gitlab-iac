package responses

type Members []struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
}
